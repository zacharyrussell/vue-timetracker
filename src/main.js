import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from './plugins/vuetify';
import App from './App.vue';
import VueFire from 'vuefire';
import store from './store';

Vue.config.productionTip = false;
Vue.use(VueFire);
Vue.use(Vuex);

new Vue({
  data: {
    timeEntries: [],
  },
  store,
  render: h => h(App),
}).$mount('#app');
