import Vue from 'vue';
import Vuex from 'vuex';
import * as Firebase from 'firebase/app';
import 'firebase/firestore';
import timeentries from './timeentries';
let config = {
  apiKey: 'AIzaSyB_Ez9NQrJeyg4wBz0ZqWCNOI708uNgGeM',
  authDomain: 'firestore-time-tracker.firebaseapp.com',
  databaseURL: 'https://firestore-time-tracker.firebaseio.com',
  projectId: 'firestore-time-tracker',
  storageBucket: 'firestore-time-tracker.appspot.com',
  messagingSenderId: '583596659349',
};
Firebase.initializeApp(config);
Vue.use(Vuex);
const state = {
  db: Firebase.firestore(),
};
export default new Vuex.Store({
  state,
  modules: {
    timeentries,
  },
});
