const state = [];
const mutations = {
  SET_ENTRY(state, {entry}) {
    const data = entry.data();
    state.push(data);
  },
  ADD_TIME(state, {project, hours, description}) {
    state.push({project, hours, description});
  },
};

const actions = {
  async get({commit, rootState}) {
    console.log('getting');
    let timeEntriesRef = rootState.db.collection('timeEntries');
    let timeEntries = await timeEntriesRef.get();
    timeEntries.forEach(entry => commit('SET_ENTRY', {entry}));
  },
  logTime({commit, rootState}, {project, hours, description}) {
    rootState.db.collection('timeEntries').add({
      project: project,
      hours: hours,
      description: description,
    });
    commit('ADD_TIME', {project, hours, description});
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
